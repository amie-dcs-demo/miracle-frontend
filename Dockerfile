FROM node:12.19.0 as builder
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build

FROM nginx:1.14-alpine

COPY --from=builder /app/dist/ /usr/share/nginx/html/
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./run.sh /run.sh

EXPOSE 80

CMD ./run.sh
